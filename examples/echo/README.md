
### Examples

The echo server/client pair is based on the examples from
Boost.Asio C++ Network Programming - Second Edition by Wisnu Anggoro, John Torjo

To run the server/client pair, you need to prepare two Linux computers with Bluetooth capabilities.

#### Build

##### Make
Build using make command. Note cant use make -j2 to use more threads as this currently interferes with
the mkdir commands. Which I should fix.

```
make
```

Note if you installed Boost manually in /usr/local/lib this may not be in the default Ubuntu path. 
If make cant find it add it at the end of the LD_LIBRARY_PATH environment variable in /etc/environment. 
Reboot and the new path will be effective.

##### CMake

```
mkdir build
cd build
cmake ..
make -j2
```

#### Run

Install this project on both of the computers. On one of the computers, run the program with:
```
./echoserver
```

Then on the other computer, run the client with:
```
./echoclient
```

You will see "Hello World!" sent to the server and echoed back to the client.