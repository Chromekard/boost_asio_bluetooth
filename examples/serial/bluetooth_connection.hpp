#pragma once

//C++
#include <deque>
#include <string>


//Libraries
#include "boost/asio.hpp"

#include "../../include/boost/asio/bluetooth/bluetooth.hpp"

///TODO: Do some poly so that this interchangable with tcp socket

/**
 * @note use enable shared from this so that object can controll its own lifecyle
*/
class BluetoothConnection : 
  public std::enable_shared_from_this<BluetoothConnection>
{
public:

  /**
   * @TODO: pass the socket components needed to create socket or socket itself
  */
  BluetoothConnection(boost::asio::io_context& context);

  /**
   * Start async reading
  */
  void Start();

  /**
   * Send message over bluetooth
   * @param msg
  */
  void Send(std::string msg);

  /**
   * 
  */
  boost::asio::bluetooth::bluetooth::socket& GetSocket()
  {
    return socket_;
  }

private:

  /**
   * 
  */
  void Read();

  /**
   * 
  */
  void ReadDone(boost::system::error_code const &error, std::size_t bytes);

  /**
   * 
  */
  void QueueMessage(std::string msg);

  /**
   * 
  */
  void SendPacket();

  /**
   * 
  */
  void SendPacketDone(boost::system::error_code const &error);


private:

  boost::asio::io_context& context_;
  boost::asio::bluetooth::bluetooth::socket socket_;
  boost::asio::io_context::strand strand_;

  ///TODO: At some point replace the basic_stream_descriptor
  /// with a real endpoint 

  boost::asio::streambuf in_packet_{};
  std::deque<std::string> out_packet_queue{};

};