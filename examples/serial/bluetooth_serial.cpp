//C++
#include <iostream>

//Linux

//Libraries
#include "boost/asio.hpp"

#include "../../include/boost/asio/bluetooth/bluetooth.hpp"
///NOTE: If the headers are installed in boost dir use the following include
/// "boost/asio_ext/bluetooth/bluetooth.hpp" instead

#include "bluetooth_acceptor.hpp"
#include "bluetooth_connection.hpp"
#include "sdp_service.hpp"

int main()
{
  ///Register a service provider, requires sudo, and make sure that 
  /// bluetooth mac address and channels match what the acceptor uses
  SdpService sdp_service{};
  if(!sdp_service.RegisterSDP("Serial", "Serial_port", "Me", 2))
  {
    std::cout << "Failed to register sdp service exiting" << std::endl;
    return 1;
  }

  std::cout << "Registered sdp service" << std::endl;

  boost::asio::io_context io_context{};
  BluetoothAcceptor acceptor{io_context};
  acceptor.StartListen(2);

  std::cout << "Started listening for connection" << std::endl;

  //
  boost::asio::signal_set signals(io_context, SIGINT, SIGTERM); //Note not thread safe
  signals.async_wait([&](boost::system::error_code ec, int sig)
  {
    if(!ec)
    {
      io_context.stop();
    }
  });

  std::cout << "Entering context" << std::endl;

  io_context.run();
  return 0;
}