#include "sdp_service.hpp"

#include <iostream>


SdpService::~SdpService()
{
  if(session_)
  {
    //Causes: double free or corruption (out)
    //sdp_close(session_);
  }
}

bool SdpService::RegisterSDP(
    const std::string sdp_service_name, 
    const std::string sdp_service_description, 
    const std::string sdp_service_provider,
    const uint8_t channel)
{
  bdaddr_t bt_device_address{{0, 0, 0, 0, 0, 0}}; //ANY ADDRESS
  bdaddr_t sdp_device_address{{0, 0, 0, 0xff, 0xff, 0xff}}; //LOCAL SDP SERVICE
  uint32_t svc_uuid_int[4] = {0x01110000, 0x00100000, 0x80000080, 0xFB349B5F}; //a1c57e78-f88c-11e8-8eb2-f2801f1b9fd1
  //UUID FOR SERIAL PORT

  return RegisterSDP(
    sdp_service_name, 
    sdp_service_description, 
    sdp_service_provider, channel,
    bt_device_address, 
    sdp_device_address, 
    svc_uuid_int);
}

bool SdpService::RegisterSDP(
    const std::string sdp_service_name, 
    const std::string sdp_service_description, 
    const std::string sdp_service_provider,
    uint8_t bt_channel,
    bdaddr_t bt_device_address,
    bdaddr_t sdp_device_address,
    uint32_t svc_uuid_int[4])
{
  // creat and set the general service ID
  uuid_t svc_uuid;
  sdp_record_t *record = sdp_record_alloc();
  if(!record)
  {
    std::cerr << "Failed to allocate sdp record" << std::endl;
    return false;
  }
  sdp_uuid128_create(&svc_uuid, &svc_uuid_int);
  sdp_set_service_id(record, svc_uuid);

  //Print uuid to text
  //char str[256] = "";
  //sdp_uuid2strn(&svc_uuid, str, 256);

  // set the service class
  uuid_t svc_class_uuid;
  sdp_uuid16_create(&svc_class_uuid, SERIAL_PORT_SVCLASS_ID);
  sdp_list_t *svc_class_list = sdp_list_append(0, &svc_class_uuid);
  if(!svc_class_list)
  {
    std::cerr << "Failed to get class list" << std::endl;
    return false;
  }
  sdp_set_service_classes(record, svc_class_list);

  // set the Bluetooth profile information
  sdp_profile_desc_t profile;
  sdp_uuid16_create(&profile.uuid, SERIAL_PORT_PROFILE_ID);
  profile.version = 0x0100;
  sdp_list_t *profile_list = sdp_list_append(0, &profile);
  if(!profile_list)
  {
    std::cerr << "Failed to get profile list" << std::endl;
    return false;
  }
  sdp_set_profile_descs(record, profile_list);

  // make the service record publicly browsable
  uuid_t root_uuid;
  sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
  sdp_list_t *root_list = sdp_list_append(0, &root_uuid);
  if(!root_list)
  {
    std::cerr << "Failed to get root list" << std::endl;
    return false;
  }
  sdp_set_browse_groups(record, root_list);

  // set l2cap information
  uuid_t l2cap_uuid;
  sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);

  sdp_list_t *l2cap_list = sdp_list_append(0, &l2cap_uuid);
  if(!l2cap_list)
  {
    std::cerr << "Failed to get l2cap list" << std::endl;
    return false;
  }

  sdp_list_t *proto_list = sdp_list_append(0, l2cap_list);
  if(!proto_list)
  {
    std::cerr << "Failed to get proto list" << std::endl;
    return false;
  }

  // register the RFCOMM channel for RFCOMM sockets
  uuid_t rfcomm_uuid;
  sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
  sdp_data_t *channel = sdp_data_alloc(SDP_UINT8, &bt_channel);
  if(!channel)
  {
    std::cerr << "Failed to allocate channel data" << std::endl;
    return false;
  }
  sdp_list_t *rfcomm_list = sdp_list_append(0, &rfcomm_uuid);
  if(!rfcomm_list)
  {
    std::cerr << "Failed to get rfcomm list" << std::endl;
    return false;
  }
  sdp_list_append(rfcomm_list, channel);
  sdp_list_append(proto_list, rfcomm_list);

  sdp_list_t *access_proto_list = sdp_list_append(0, proto_list);
  if(!access_proto_list)
  {
    std::cerr << "Failed to get access protocol list" << std::endl;
    return false;
  }
  sdp_set_access_protos(record, access_proto_list);

  // set the name, provider, and description
  sdp_set_info_attr(record, sdp_service_name.c_str(), 
    sdp_service_provider.c_str(), 
    sdp_service_description.c_str());

  /// connect to the local SDP server, NOTE: requires sudo to work
  sdp_session_t *session_ = sdp_connect(&bt_device_address, 
    &sdp_device_address, SDP_RETRY_IF_BUSY);
  if(!session_)
  {
    std::cerr << "Failed to connect to local SDP server" << std::endl;
    return false;
  }

  //Register record
  if (sdp_record_register(session_, record, 0) < 0)
  {
    std::cerr << "Failed to register spd record" << std::endl;
    return false;
  }

  ///TODO: Might need to do this running as memory might leak on errors

  // cleanup
  sdp_data_free(channel);
  sdp_list_free(l2cap_list, 0);
  sdp_list_free(rfcomm_list, 0);
  sdp_list_free(root_list, 0);
  sdp_list_free(access_proto_list, 0);
  sdp_list_free(svc_class_list, 0);
  sdp_list_free(profile_list, 0);

  return true;
}