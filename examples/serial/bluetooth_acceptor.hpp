#pragma once

//Libraries
#include "boost/asio.hpp"


//Local
#include "bluetooth_connection.hpp"

/**
 * 
*/
class BluetoothAcceptor
{
public:

  /**
   * 
  */
  BluetoothAcceptor(boost::asio::io_context& context);

  /**
   * 
  */
  void StartListen(const uint8_t channel);

  /**
   * 
  */
  void StartListen(const std::string& mac_addr, const uint8_t channel);

private:

  /**
   * 
  */
  void HandleConnection(std::shared_ptr<BluetoothConnection> handler,
    boost::system::error_code const& error);

private:

  boost::asio::io_context& context_;
  boost::asio::bluetooth::bluetooth::acceptor acceptor_;
  
};
