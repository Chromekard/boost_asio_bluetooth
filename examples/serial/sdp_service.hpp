#pragma once

#include <string>
#include <memory>

#include "bluetooth/sdp.h"
#include "bluetooth/sdp_lib.h"

/**
 * Wrapper class for sdp service
 * Place this in a unique pointer to prevent
 * destruction to early
*/
class SdpService
{
public:

  /**
   * 
  */
  SdpService(){};

  //Dont allow copy
  SdpService(SdpService const&) = delete;
  SdpService& operator=(SdpService const&) = delete;

  ~SdpService();

  /**
   * Register service discovery protocol
   * @param sdp_service_name
   * @param sdp_service_description
   * @param sdp_service_provider
   * @return true if session was created, false if not
  */
  bool RegisterSDP(
    const std::string sdp_service_name, 
    const std::string sdp_service_description, 
    const std::string sdp_service_provider,
    const uint8_t channel);

  /**
   * Register service discovery protocol
   * @param bt_device_address 
   * @param sdp_device_address
   * @param bt_channel that the bluetooth will use
   * @param svc_uuid_int - 
   * @param sdp_service_description
   * @param sdp_service_provider
   * @return true if session was created, false if not
  */
  bool RegisterSDP(
    const std::string sdp_service_name, 
    const std::string sdp_service_description, 
    const std::string sdp_service_provider,
    uint8_t bt_channel,
    bdaddr_t bt_device_address,
    bdaddr_t sdp_device_address,
    uint32_t svc_uuid_int[4]);

private:

  sdp_session_t * session_;
};
