#include "bluetooth_connection.hpp"

#include <istream>
#include <iostream>


BluetoothConnection::BluetoothConnection(boost::asio::io_context& context) : 
  context_(context), socket_(context), strand_(context)
{
  ///TODO: Get socket?
  //socket_.assign(natsock);
}

void BluetoothConnection::Start()
{
  Read();
}

void BluetoothConnection::Send(std::string msg)
{
  boost::asio::post(context_, strand_.wrap( 
    [me = shared_from_this(), msg]()
    {
      me->QueueMessage(msg);
    }));
}

void BluetoothConnection::Read()
{
  boost::asio::async_read_until( socket_, in_packet_, '#', 
    [me = shared_from_this()]
    (boost::system::error_code const &error, std::size_t bytes)
    {
      me->ReadDone(error, bytes);
    });
}

void BluetoothConnection::ReadDone(boost::system::error_code const &error, std::size_t bytes)
{
  if(error)
  {
    return;
  }

  std::istream stream(&in_packet_);
  std::string packet_string{};
  stream >> packet_string;

  ///TODO: Trigger callback with packet string
  std::cout << "Packet: " << packet_string << std::endl;

  ///Start next async read
  Read();
}

void BluetoothConnection::QueueMessage(std::string msg)
{
  bool writing = !out_packet_queue.empty();
  out_packet_queue.push_back(msg);
  
  if(!writing)
  {
    SendPacket();
  }
}

void BluetoothConnection::SendPacket()
{
  out_packet_queue.front() += '\0';
  boost::asio::async_write(socket_, boost::asio::buffer(out_packet_queue.front()), 
    strand_.wrap([me = shared_from_this()]
    (boost::system::error_code const &error, std::size_t bytes)
    {
      me->SendPacketDone(error);
    }));
}

void BluetoothConnection::SendPacketDone(boost::system::error_code const &error)
{
  if(!error)
  {
    out_packet_queue.pop_front();
    if(!out_packet_queue.empty())
    {
      SendPacket();
    }
  }
}
