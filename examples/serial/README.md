
### Bluetooth Serial Example

For more information on Service Discovery Protocols and Bluetooth in general take a look at Albert Huang booklet [An Introduction to Bluetooth Programming](https://people.csail.mit.edu/albert/bluez-intro/index.html)

#### Preperation

Make sure that your bluetooth is on and discoverable.
To use SDP your bluetooth deamon needs to be running in compatability mode.

To find the deamon execution use the following command:
```
sudo systemctl status bluetooth
```

Under the Drop-In category look for the config file sited for running the daemon. This is the file you will want to edit.
Open the config in a text editor using sudo. Then Add “-C” to this line in the service:
```
ExecStart=/usr/lib/bluetooth/bluetoothd
```
Additionally “-d” can be added for debugging

Restart bluetooth service:
```
sudo systemctl daemon-reload
sudo systemctl restart bluetooth
sudo service bluetooth start
sudo hciconfig -a hci0 reset
```


#### Build

###### Make
Build using make command. Note cant use make -j2 to use more threads as this currently interferes with
the mkdir commands. Which I should fix.

```
make
```

Note if you installed Boost manually in /usr/local/lib this may not be in the default Ubuntu path. 
If make cant find it add it at the end of the LD_LIBRARY_PATH environment variable in /etc/environment. 
Reboot and the new path will be effective.

###### CMake

```
mkdir build
cd build
cmake ..
make -j2
```

#### Run

Install this project on the computer. In the bin/ or build/ directory, do:
```
sudo ./bluetooth_serial
```
Root access is required to create the sdp service.
Connect with another device to the serial port, and any sent data should be printed to console.
Such as the android app "Bluetooth Serial"