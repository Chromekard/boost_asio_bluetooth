#include "bluetooth_acceptor.hpp"

#include <iostream>

BluetoothAcceptor::BluetoothAcceptor(boost::asio::io_context &context) : 
  context_(context), acceptor_(context)
{
  ///TODO: Assing socket
  //acceptor_.assign(AF_INET, fd);
}

void BluetoothAcceptor::StartListen(const uint8_t channel)
{
  boost::asio::bluetooth::bluetooth::socket socket{context_};
  boost::asio::bluetooth::bluetooth::acceptor acceptor{context_};
	boost::asio::bluetooth::bluetooth::endpoint endpoint(channel);
	acceptor_.open(endpoint.protocol());
	acceptor_.bind(endpoint);
	acceptor_.listen(boost::asio::socket_base::max_connections);

  auto handler = std::make_shared<BluetoothConnection>(context_);
  acceptor_.async_accept(handler->GetSocket(), 
    [=](auto ec)
    {
      HandleConnection(handler, ec);
    });
}

void BluetoothAcceptor::StartListen(const std::string& mac_addr, const uint8_t channel)
{
  boost::asio::bluetooth::bluetooth::socket socket{context_};
  boost::asio::bluetooth::bluetooth::acceptor acceptor{context_};
	boost::asio::bluetooth::bluetooth::endpoint endpoint(mac_addr, channel);
	acceptor_.open(endpoint.protocol());
	acceptor_.bind(endpoint);
	acceptor_.listen(boost::asio::socket_base::max_connections);

  auto handler = std::make_shared<BluetoothConnection>(context_);
  acceptor_.async_accept(handler->GetSocket(), 
    [=](auto ec)
    {
      HandleConnection(handler, ec);
    });
}

void BluetoothAcceptor::HandleConnection(
  std::shared_ptr<BluetoothConnection> handler,
  boost::system::error_code const& error)
{
  if(error)
  {
    std::cerr << "Error on accept: " << error.message() << std::endl;
    return;
  }

  ///TODO: Possibly add the handler to some storage container for outside access
  ///Or for adding a callback
  std::cout << "Accepting connection..." << std::endl;
  handler->Start();

  auto new_handler = std::make_shared<BluetoothConnection>(context_);
  acceptor_.async_accept(new_handler->GetSocket(), 
    [=](auto ec)
    {
      HandleConnection(handler, ec);
    });
}