# boost_asio_bluetooth
This project extend the Boost ASIO functionality by adding bluetooth. By implementing a protocol and endpoint classes. This repository is a fork of [boost_asio_bluetooth](https://github.com/datengx/boost_asio_bluetooth) by [Da Teng](https://github.com/datengx). For more information regarding adding other types of protocol to the Boost ASIO library, check [link](https://www.boost.org/doc/libs/1_78_0/doc/html/boost_asio/overview/networking/other_protocols.html).

This is inteded to run on linux platforms so dont include these headers in a windows machine.
As both Bluez is linux specific.

### Install Dependencies

Start by installing the following dependencies:

##### Boost Library
```
sudo apt-get install libboost-all-dev
```

Boost can also be installed manually. If you want a specific version.

##### BlueZ

Install

```
sudo apt-get install bluez
```
If you have older version of BlueZ installed, checkout this [link](https://askubuntu.com/questions/662347/bluez-5-x-and-ubuntu-14-0x/662349).

### Build

The library is header only so no need to build anything other than the examples

### Install

Since the library is header only we only need to copy the boost folder where we want it.

```
sudo cp -r /home/user/path/boost_asio_proto_extension/include/boost/ /usr/include/
```

This would be if we wanted to install the headers globaly. Alternativly you could just add the folder
to your own projects include folder. Or is you installed boost manually then you could install into:

```
/usr/local/include/
```

then the files will be in the same directory as boost.

##### Uninstall

To uninstall you just need to delete the header files. In this case you would need to find where you
copied the header files and recursivly delete the bluetooth folder.

```
sudo rm -r /usr/include/boost/asio/bluetooth
```

### Include

Now this would depend on if you have installed the headers in the same folder as boost.
If you have then including the boost headers should be all you need to do.

##### Make
For make you realy only need to point to the headers:

INC= -I/usr/include/boost/asio/bluetooth

##### CMake

For cmake we only need to find boost and bluez. Check the cmakes in the examples to see how this is done.